import './App.scss';
import { connect } from 'react-redux';
import { Container, Box, ThemeProvider, createMuiTheme, Typography } from '@material-ui/core';
import RadioButtonGroup from './components/RadioButtonGroup';
import NDFLSwitch from './components/NDFLSwitch';
import CalculationBox from './components/CalculationBox';
import SumInput from './components/SumInput';
import grey from '@material-ui/core/colors/grey';

const defaultRadioButtons = [
    {
        type: 'radio',
        text: 'Оклад за месяц',
    },
    {
        type: 'radio',
        text: 'МРОТ',
        hint: 'МРОТ — минимальный размер оплаты труда. Разный для разных регионов.'
    },
    {
        type: 'radio',
        text: 'Оплата за день',
    },
    {
        type: 'radio',
        text: 'Оплата за час',
    }
];

const theme = createMuiTheme({
    palette: {
        primary: {
            main: grey[900]
        },
        secondary: {
            main: '#f8a23f',
            contrastText: grey[500]
        }
    },
    overrides: {
        MuiRadio: {
          root: {
            padding: 3
          }
        }
      }
});

function App({ checkedButton }) {

    return (
        <ThemeProvider theme={theme}>
            <div className='App'>
                <Container className='mt-4'>
                    <Box width={400}>
                        <Typography fontSize='11pt' fontWeight='bold' color='textSecondary'>Сумма</Typography>
                        <RadioButtonGroup buttons={defaultRadioButtons} />
                        <Box ml={4}>
                            <NDFLSwitch />
                            <SumInput />
                        </Box>
                        {checkedButton === defaultRadioButtons[0].text ? <CalculationBox /> : ''}
                    </Box>
                </Container>
            </div>
        </ThemeProvider>
    );
}

const mapState = state => ({
    checkedButton: state.checkedButton,
    notIncludeNDFL: state.notIncludeNDFL
});

export default connect(mapState)(App);
