import { RadioGroup, Grid } from '@material-ui/core';
import RadioButton from './RadioButton';
import { connect } from 'react-redux'

const RadioButtonGroup = ({ buttons, checkedButton, setCheckedButton }) => {
    const updateButtons = (e) => {
        setCheckedButton(e.target.value);
    }

    return (
        <RadioGroup value={checkedButton} onChange={updateButtons}>
            {buttons.map((item, idx) => (
                <Grid container key={`${idx}-default`}>
                    <RadioButton item={item} idx={idx} />
                </Grid>
            ))}
        </RadioGroup>
    );
};

const mapState = state => ({
    checkedButton: state.checkedButton,
})

const mapDispatch = dispatch => ({
    setCheckedButton: dispatch.checkedButton.set,
})

export default connect(mapState, mapDispatch)(RadioButtonGroup)