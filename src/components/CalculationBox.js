import { Box, Grid } from '@material-ui/core';
import { connect } from 'react-redux'
import CalculationRow from './CalculationRow';

const CalculationBox = ({ sum, notIncludeNDFL }) => {
    return (
        <Box mt={2}>
            <Grid container direction='column'>
                <Box bgcolor='#fbf4da' p={2} borderRadius={5}>
                    <CalculationRow sum={!notIncludeNDFL ? Math.floor(sum * 0.87) : sum} text='Р сотрудник будет получать на руки' />

                    <CalculationRow sum={!notIncludeNDFL ? Math.floor(sum * 0.13) : Math.floor(sum / 0.87 - sum)} text='Р НДФЛ, 13% от оклада' />

                    <CalculationRow sum={notIncludeNDFL ? Math.floor(sum / 0.87) : sum} text='Р за сотрудника в месяц' />
                </Box>
            </Grid>
        </Box>
    )
}

const mapState = state => ({
    notIncludeNDFL: state.notIncludeNDFL,
    sum: state.sum
});

export default connect(mapState)(CalculationBox);