import { FormControlLabel, TextField, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux'

const SumInput = ({ sum, setSum }) => {

    const inputTextTheme = makeStyles((theme) => ({
        root: {
            fontSize: 16,
            width: 'auto',
            margin: '5px 10px',
            [`& fieldset`]: {
                borderRadius: 20,
            },
        }
    }))();


    const updateSum = (e) => {
        setSum(Number(e.target.value));
    }

    return (
        <FormControlLabel control={<TextField id='sum-input' variant='outlined' classes={inputTextTheme} size='small' onChange={updateSum} value={sum.toLocaleString('ru-RU')} color='secondary' />} label={'Р'} />
    );
};

const mapState = state => ({
    sum: state.sum,
})

const mapDispatch = dispatch => ({
    setSum: dispatch.sum.set,
})

export default connect(mapState, mapDispatch)(SumInput)