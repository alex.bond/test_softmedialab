import { Grid, Typography } from '@material-ui/core';

const CalculationRow = (props) => {
    return (
        <Grid container spacing={1}>
            <Grid item><Typography fontWeight='bold'>{props.sum.toLocaleString('ru-RU')}</Typography></Grid>
            <Grid item><Typography fontWeight='bold'>{props.text}</Typography></Grid>
        </Grid>
    )
}

export default CalculationRow