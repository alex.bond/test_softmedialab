import { Grid, Switch, Typography, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';

const NDFLSwitch = ({ notIncludeNDFL, setNotIncludeNDFL }) => {
    const updateSwitch = (e) => {
        setNotIncludeNDFL(e.target.checked);
    }

    const switchTheme = makeStyles((theme) => ({
        root: {
            width: 36,
            height: 20,
            padding: 0,
            margin: theme.spacing(1),
        },
        switchBase: {
            padding: 1,
            '&$checked': {
                transform: 'translateX(16px)',
                color: theme.palette.common.white,
                '& + $track': {
                    backgroundColor: 'secondary',
                    opacity: 1,
                    border: 'none',
                },
            },
            '&$focusVisible $thumb': {
                color: 'secondary',
                border: '6px solid #fff',
            },
        },
        thumb: {
            width: 18,
            height: 18,
        },
        track: {
            borderRadius: 20 / 2,
            backgroundColor: theme.palette.grey[300],
            opacity: 1,
            transition: theme.transitions.create(['background-color', 'border']),
        },
        checked: {},
        focusVisible: {}
    }))();


    return (
        <Typography component='div'>
            <Grid component='label' container alignItems='center' spacing={1}>
                <Grid item><Typography fontSize='10pt' fontWeight='bold' color={notIncludeNDFL ? 'textSecondary' : 'primary'}>Указать с НДФЛ</Typography></Grid>
                <Grid item>
                    <Switch
                        classes={{
                            root: switchTheme.root,
                            switchBase: switchTheme.switchBase,
                            thumb: switchTheme.thumb,
                            track: switchTheme.track,
                            checked: switchTheme.checked,
                        }}
                        checked={notIncludeNDFL}
                        onChange={updateSwitch}
                    />
                </Grid>
                <Grid item><Typography fontSize='10pt' fontWeight='bold' color={!notIncludeNDFL ? 'textSecondary' : 'primary'}>Без НДФЛ</Typography></Grid>
            </Grid>
        </Typography>
    );
}

const mapState = state => ({
    notIncludeNDFL: state.notIncludeNDFL,
})

const mapDispatch = dispatch => ({
    setNotIncludeNDFL: dispatch.notIncludeNDFL.set,
})

export default connect(mapState, mapDispatch)(NDFLSwitch);