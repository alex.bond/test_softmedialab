import { FormControlLabel, Box, Popover, Radio, makeStyles, Typography } from '@material-ui/core';
import InfoOutlined from '@material-ui/icons/InfoOutlined';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';

import { useState } from 'react';

const RadioButton = (props) => {
    const [anchorEl, setAnchorEl] = useState(null);
    const [clickOpened, setClickOpened] = useState(false);

    const { item, idx } = props;

    const popoverTheme = makeStyles(_ => ({
        root: {
            pointerEvents: 'none'
        },
        paper: {
            marginTop: '-8px',
            background: 'none'
        }
    }))();

    const handlePopoverOpen = (event) => {
        if (event.type === 'click') {
            setClickOpened(true);
        }
        setAnchorEl(event.currentTarget);
    };

    const handlePopoverClose = () => {
        if (!clickOpened) {
            setAnchorEl(null);
        }
    };

    const handlePopoverToggle = (event) => {
        if (clickOpened) {
            setAnchorEl(null);
            setClickOpened(false);
        } else {
            handlePopoverOpen(event);
        }
    };

    return (
        <>
            <FormControlLabel
                value={item.text}
                label={<Typography fontWeight='bold'>{item.text}</Typography>}
                fontWeight={700}
                control={<Radio disableRipple color='secondary' />}

                id={`${idx}-default`}
            />
            {
                item.hint ?
                    <Box onMouseEnter={handlePopoverOpen}
                        onMouseLeave={handlePopoverClose}
                        onClick={handlePopoverToggle}>
                        {clickOpened ? <CancelOutlinedIcon fontSize="small" /> : <InfoOutlined fontSize="small" />}
                        <Popover
                            classes={popoverTheme}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={!!anchorEl}
                            anchorEl={anchorEl}
                            disableRestoreFocus
                            elevation={0}
                        >
                            <Box style={{
                                content: '""',
                                display: 'block',
                                width: '0',
                                height: '0',
                                borderRight: '12px solid transparent',
                                borderBottom: '18px solid #5e6cab',
                                left: 'calc(50% - 10px)',
                                background: 'none'
                            }} />
                            <Box bgcolor='#5e6cab' color='white' borderRadius='0px 4px 0px 0px' p={3}>
                                <Typography>{item.hint}</Typography>
                            </Box>
                        </Popover>
                    </Box> : ''
            }
        </>)
}

export default RadioButton