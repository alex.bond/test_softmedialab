import { init } from '@rematch/core'

const checkedButton = {
    state: '',
    reducers: {
        set(_, payload) {
            return payload
        },
    }
}

const notIncludeNDFL = {
    state: false,
    reducers: {
        set: (_, payload) => payload,
    }
}

const sum = {
    state: 40000,
    reducers: {
        set: (_, payload) => payload,
    }
}

const models = {
    checkedButton,
    notIncludeNDFL,
    sum
}

const store = init({
    models,
});

export default store